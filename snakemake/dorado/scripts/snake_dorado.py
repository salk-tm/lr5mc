from loreme import (download_dorado, check_gpu_availability, dorado_basecall)

download_dorado(snakemake.params.platform)
check_gpu_availability([0])
dorado_basecall(snakemake.input.pod5, snakemake.output.bam, speed=snakemake.params.speed,
    accuracy=snakemake.params.accuracy, frequency=snakemake.params.frequency,
    modified_bases=snakemake.params.modified_bases)
