from loreme import (download_pbcpg, check_tags, pbcpg_align_bam,
                   pbcpg_pileup, PBCPG_MODEL)

check_tags(snakemake.input.bam, n_reads=snakemake.params.n_reads)
download_pbcpg()
pbcpg_align_bam(snakemake.input.fasta, snakemake.input.bam,
    snakemake.output.bam, threads=snakemake.threads,
    memory_mb = max(floor(snakemake.resources.mem_mb/snakemake.threads), 1))
pbcpg_pileup(snakemake.output.bam, snakemake.output.prefix,
    hap_tag=snakemake.params.hap_tag,
    min_coverage=snakemake.params.min_coverage,
    min_mapq=snakemake.params.min_mapq,
    model=PBCPG_MODEL,
    threads=snakemake.threads)
