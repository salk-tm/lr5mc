.. _pb_postprocessing:

Pacific Biosciences reads
=========================

Download pb-CpG-tools
---------------------

Extracting methylation calls from PB reads requires  `pb-CpG-tools <https://github.com/PacificBiosciences/pb-CpG-tools>`_ . Download it by running

.. code-block:: zsh

   loreme download-pbcpg

.. note::

   Aligning PB reads requires you to have `pbmm2 <https://github.com/PacificBiosciences/pbmm2>`_ installed. This is already done if you use the conda environment setup described in :doc:`installation`.

.. role:: zsh(code)
   :language: zsh

Download example datasets
-------------------------

Use :zsh:`loreme download-pb-example` to download the GRCh38 human genome build, RefSeq annotations, and an unaligned BAM file containing HiFi reads.

.. note::

  This example analysis involves downloading and processing a 21 GB BAM file of HiFi reads, which will be time consuming and resource-intensive. For that reason, it is recommended to reproduce it on a remote server (such as Seabiscuit for members of the `Michael Lab <https://michael.salk.edu/>`_ at Salk).

.. code-block:: zsh

   mkdir loreme_example_GRCh38/
   loreme download-pb-example -d loreme_example_GRCh38/

Consider going for a coffee break with your colleagues, as this will take a few
minutes. Once the download is complete, the example datasets will be present
in the desintation directory.

.. code-block:: zsh

   cd loreme_example_GRCh38/
   ls

.. code-block:: zsh

   GCA_000001405.15_GRCh38_full_analysis_set.refseq_annotation.gff.gz
   GCA_000001405.15_GRCh38_no_alt_analysis_set.fa
   m64011_190830_220126.hifi_reads.bam

These are the GRCh38 human reference genome in FASTA format, a GFF3 file of annotations, and a BAM file of HiFi reads. To extract methylation calls from the HiFi reads, use :zsh:`loreme pbcpg` to run the full pipeline.

Check BAM file for MM/ML tags
-----------------------------

Before processing with pbcpg, check that the BAM file contains MM/ML tags.

.. code-block:: zsh

   loreme check-tags m64011_190830_220126.hifi_reads.bam


Extract methylation calls
-------------------------

.. code-block:: zsh

   loreme pbcpg --threads 4 --memory 64000 \
     m64011_190830_220126.hifi_reads.bam \
     GCA_000001405.15_GRCh38_no_alt_analysis_set.fa \
     hifi_example

This step may take a matter of hours or days depending on your computing resources. Once it is complete, a set of output files will be written to the example directory:

.. code-block:: zsh

 hifi_example-aligned_bam_to_cpg_scores.log
 hifi_example.combined.bed
 hifi_example.combined.bw
 hifi_example.pbmm2.bam
 hifi_example.pbmm2.bam.bai


The :zsh:`.pbmm2.bam` file contains the aligned reads. The :zsh:`.bed` file contains the methylation calls.

Postprocessing
--------------

**Calculate mean methylation level**

To calculate the mean methylation level of all cytosines:

.. code-block:: zsh

   loreme mean --total hifi_example.combined.bed

.. code-block:: zsh

   Group	Methylation level (%)
   0	68.05964065210246

.. note::

   This and subsequent steps represent postprocessing that can be applied to
   either Pacific Biosciences or Oxford Nanopore datasets.

To calculate mean methylation of specific chromosomes and generate a barplot:

.. code-block:: zsh

   loreme mean hifi_example.combined.bed --chromosomes \
     chr1 chr2 chr3 chr4 chr5 chr6 chr7 chr8 chr9 chr10 chr11 \
     chr12 chr13 chr14 chr15 chr16 chr17 chr18 chr19 chr20 chr21 \
     chr22 chrX chrY \
     --plot hifi_example_mean.svg

.. code-block:: none

   Chromosome	Group	Methylation level (%)
   chr1	0	67.82360570377412
   chr10	0	68.30633364307305
   chr11	0	66.56842885050763
   chr12	0	68.92853188413342
   chr13	0	67.99855596247355
   chr14	0	67.86415061100217
   chr15	0	68.19070720349553
   chr16	0	68.73442341618528
   chr17	0	68.74986311738878
   chr18	0	67.99331555563278
   chr19	0	68.11691853818014
   chr2	0	68.55852677486237
   chr20	0	66.8569883632415
   chr21	0	66.96363386076278
   chr22	0	68.89587082522333
   chr3	0	68.43344623810002
   chr4	0	67.65825920035176
   chr5	0	67.68864941276578
   chr6	0	68.23425386832164
   chr7	0	69.12754750492593
   chr8	0	67.24530166215865
   chr9	0	68.23987826992321
   chrX	0	68.72026847882196
   chrY	0	65.39801039125169

.. figure:: _static/hifi_example_mean.*
   :alt: Mean methylation levels across human chromosomes.

   Mean methylation levels across human chromosomes.

**Methylation level across chromosomes**

To plot methylation across a chromosome:

.. code-block:: zsh

   loreme plot --reference GCA_000001405.15_GRCh38_no_alt_analysis_set.fa \
     --chromosomes chr1 --bin-size 1 --x-label "Chromosome 1" \
     hifi_example.combined.bed hifi_example_chr1.svg

.. figure:: _static/hifi_example_chr1.*
   :alt: Methylation levels across CRCh38 chr1.

   Methylation levels across CRCh38 chr1.

Multiple chromosomes may be included in a single plot.

.. code-block:: zsh

   loreme plot --reference GCA_000001405.15_GRCh38_no_alt_analysis_set.fa \
     --chromosomes chr1 chr2 chr3 --bin-size 1 \
     hifi_example.combined.bed hifi_example_chr1.svg

.. figure:: _static/hifi_example_chr1_3.*
   :alt: Methylation levels across CRCh38 chromosomes 1-3.

   Methylation levels across CRCh38 chromosomes 1-3.

**Methylation level of promoters**

To calculate methylation levels of promoter regions:

.. code-block:: zsh

   loreme promoter GCA_000001405.15_GRCh38_full_analysis_set.refseq_annotation.gff.gz \
     hifi_example.combined.bed --hist hifi_example_hist_promoter.svg > hifi_example_promoter.bed
   head hifi_example_promoter.bed

.. code-block:: none

   chr1	17436	19436	gene-MIR6859-1	88.17	-
   chr1	27774	29774	gene-MIR1302-2HG	5.55	+
   chr1	28366	30366	gene-MIR1302-2	6.61	+
   chr1	36081	38081	gene-FAM138A	54.47	-
   chr1	175246	177246	gene-LOC124900384	73.05	-
   chr1	180105	182105	gene-LOC124903814	72.43	-
   chr1	200117	202117	gene-LOC100996442	50.86	-
   chr1	271223	273223	gene-LOC124903815	59.65	+
   chr1	382235	384235	gene-LOC112268260	72.63	-
   chr1	495445	497445	gene-LOC100132287	85.01	-

.. figure:: _static/hifi_example_hist_promoter.*
   :alt: Histogram of methylation levels of promoter regions.

   Histogram of methylation levels of promoter regions.

**Methylation level of gene bodies**

To calculate methylation levels of gene bodies:

.. code-block:: zsh

   loreme gene-body GCA_000001405.15_GRCh38_full_analysis_set.refseq_annotation.gff.gz \
     hifi_example.combined.bed --hist hifi_example_hist_gene_body.svg > hifi_example_gene_body.bed
   head hifi_example_gene_body.bed

.. code-block:: none

   chr1	17369	17436	gene-MIR6859-1	95.85	-
   chr1	29774	35418	gene-MIR1302-2HG	51.73	+
   chr1	30366	30503	gene-MIR1302-2	45.00	+
   chr1	34611	36081	gene-FAM138A	50.45	-
   chr1	95222	107727	gene-LOC124903816	60.85	+
   chr1	120849	175246	gene-LOC124900384	76.55	-
   chr1	134773	140566	gene-LOC729737	86.18	-
   chr1	177308	180105	gene-LOC124903814	48.81	-
   chr1	184916	200117	gene-LOC100996442	59.04	-
   chr1	273223	284291	gene-LOC124903815	73.78	+

.. figure:: _static/hifi_example_hist_gene_body.*
   :alt: Histogram of methylation levels of gene bodies.

   Histogram of methylation levels of gene bodies.

**Gene methylation profile**

To plot a profile of methylation across gene bodies:

.. code-block:: zsh

   loreme plot-genes GCA_000001405.15_GRCh38_full_analysis_set.refseq_annotation.gff.gz \
     hifi_example.combined.bed hifi_example_genes.svg

.. figure:: _static/hifi_example_genes.*
   :alt: Methylation profile across gene bodies.

   Methylation profile across gene bodies.
