Installation
============

.. role:: zsh(code)
   :language: zsh

In a Conda environment
----------------------

The recommended way to install LoReMe is with a dedicated :zsh:`conda` environment:

First create an environment including all dependencies:

.. code-block:: zsh

   conda create -n loreme -c conda-forge -c bioconda samtools pbmm2 \
     urllib3 pybedtools gff2bed seaborn pyfaidx psutil gputil tabulate \
     cython h5py iso8601 more-itertools tqdm gitpython
   conda activate loreme

Then install with :zsh:`pip`:

.. code-block:: zsh

   pip install loreme

You may also wish to install :zsh:`nvtop` to monitor GPU usage:

.. code-block:: zsh

   conda install -c conda-forge nvtop

With pip
--------

.. code-block:: zsh

   pip install loreme

Check installation
------------------

Check that the correct version was installed with :zsh:`loreme --version`

Uninstall
---------

To uninstall loreme:

.. code-block:: zsh

   loreme clean
   pip uninstall loreme
