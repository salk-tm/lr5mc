LoReMe documentation
====================

Overview
--------

.. automodule:: loreme

Source code
-----------

See source code at `https://gitlab.com/salk-tm/loreme <https://gitlab.com/salk-tm/loreme>`_.

.. toctree::

   installation
   pb_reads
   ont_reads
   usage
   snakemake

.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
