Snakemake
=========

Example snakemake workflows are available on `gitlab <https://gitlab.com/salk-tm/loreme/-/tree/main/snakemake>`_ .

Pacific Biosciences reads
-------------------------

.. code-block:: snakemake

   from snakemake.utils import validate
   print(json.dumps(config, indent=4))
   validate(config, "config/config.schema.json")
   print(json.dumps(config, indent=4))
   
   rule run_pbcpg:
       input:
           fasta = config["ref_fasta"]
           bam = config["in_bam"]
       output:
           bam = f"{config['output_prefix']}.pbmm2.bam"
           bed = f"{config['output_prefix']}.combined.bed"
           bw = f"{config['output_prefix']}.combined.bw"
       conda:
           "config/requirements.yml"
       params:
           hap_tag = config["hap_tag"],
           min_coverage = config["min_coverage"]
           min_mapq = config["min_mapq"],
           output_prefix = config["output_prefix"]
       threads:
           config["threads"]
       resources:
           disk_mb = config["disk_mb"],
           mem_mb = config["mem_mb"]
       script:
           "scripts/snake_pbcpg.py"

Oxford Nanopore reads
---------------------

.. code-block:: snakemake

   from snakemake.utils import validate
   print(json.dumps(config, indent=4))
   validate(config, "config/config.schema.json")
   print(json.dumps(config, indent=4))
   
   rule run_dorado:
       input:
           pod5 = config["input_pod5"]
       output:
           bam = config["output_bam"]
       conda:
           "config/requirements.yml"
       params:
           platform = config["platform"]
           speed = config["speed"]
           accuracy = config["accuracy"]
           frequency = config["frequency"]
           modified_bases = config["modified_bases"]
       resources:
           disk_mb = config["disk_mb"],
           mem_mb = config["mem_mb"]
       script:
           "scripts/snake_dorado.py"

