Oxford Nanopore reads
=====================

.. role:: zsh(code)
   :language: zsh

Download example dataset
------------------------

Use :zsh:`loreme download-ont-example` to download a *K. azureus* (zebra perch) genome assembly, gene annotations, and a POD5 file containing ONT reads.

.. note::

   This example analysis involves downloading and processing a 26 GB POD5 file of nanopore reads, which will be time consuming and resource-intensive. For that reason, it is recommended to reproduce it on a remote server (such as Seabiscuit for members of the Michael Lab at Salk).

.. code-block:: zsh

   mkdir loreme_example_Kazu/
   loreme download-ont-example -d loreme_example_Kazu/

Consider going for a coffee break with your colleagues, as this will take a few minutes. Once the download is complete, the example datasets will be present in the desintation directory.

.. code-block:: zsh

   cd loreme_example_Kazu/
   ls

.. code-block:: none

   Kazu_090722.primary_high_confidence.gff3.gz
   Kazu_090722.softmasked.fasta.gz
   Kazu_090722_95etoh_22C_6w_SRE1_deep.pod5


Download dorado
---------------

Calling methylation from ONT long reads requires the basecaller `dorado <https://github.com/nanoporetech/dorado>`_ . Download it by running:

.. code-block:: zsh

   loreme download-dorado <platform>

This will download dorado and several basecalling models. The platform should be one of: :zsh:`linux-x64`, :zsh:`linux-arm64`, :zsh:`osx-arm64`, :zsh:`win64`, whichever matches your system. Running :zsh:`loreme download-dorado --help` will show a hint as to the correct choice.

.. note::

   For members of `Michael Lab <https://michael.salk.edu/>`_ at Salk running on seabiscuit, use :zsh:`loreme download-dorado linux-x64`.

Modified basecalling
--------------------

.. note::

   If you have FAST5 data, convert it to POD5 using :zsh:`loreme dorado-convert` (see :zsh:`loreme dorado-convert --help`)

The POD5 file will need to be contained in its own directory on a scratch disk 
to be basecalled, so create a directory and move it there:

.. note::

   Since basecalling ONT data is disk-read intensive, it will be slow on a spinning disk. The environment variable :zsh:`$SCRATCH` should be set to a location on a fast SSD or NVMe disk for the following steps. For Michael lab members running on Seabiscuit, use this command: :zsh:`SCRATCH=/scratch/<username>`.

.. code-block:: zsh

   mkdir $SCRATCH/Kazu_090722_pod5/
   mv Kazu_090722_95etoh_22C_6w_SRE1_deep.pod5 $SCRATCH/Kazu_090722_pod5/

First check that a GPU is available to use:

.. code-block:: zsh

   loreme check-gpu


You can carry out modified basecalling (i.e. DNA methylation) with default parameters by running:

.. code-block:: zsh

   loreme dorado-basecall $SCRATCH/Kazu_090722_pod5/ $SCRATCH/Kazu_090722.bam

The input argument should be a directory containing one or more POD5 files. The output argument is a BAM file containing MM/ML tags. For other parameter options, see :zsh:`loreme dorado-basecall --help`

.. note::

   By default, LoReMe uses the "fast" basecalling model, which is less accurate. In practice you will probably want to use a higher accuracy model by setting the :zsh:`--accuracy` option, e.g. :zsh:`loreme dorado-basecall --accuracy sup $SCRATCH/Kazu_090722_pod5/ $SCRATCH/Kazu_090722.bam`

.. note::

   While basecalling, tools like :zsh:`htop` or :zsh:`mutil` may show your memory usage increasing to hundreds of gigabytes. This is a bug in dorado and/or how Linux communicates with the GPU, and does not reflect actual memory usage.

To run dorado with only regular basecalling, use the :zsh:`--no-mod` option:

.. code-block:: zsh

   loreme dorado-basecall --no-mod <pod5s/> <output.bam>

Alignment
---------
The BAM file produced by dorado can be aligned to a reference index (FASTA or MMI file) with :zsh:`loreme dorado-align`:

.. code-block:: zsh

   loreme dorado-align Kazu_090722.softmasked.fasta.gz $SCRATCH/Kazu_090722.bam Kazu_090722_aligned.bam

Download modkit
---------------

Piling up methylation calls from BAM data requires `modkit <https://github.com/nanoporetech/modkit>`_ . Download it by running:

.. code-block:: zsh

   loreme download-modkit

Check BAM file for MM/ML tags
-----------------------------

Before processing with modkit, check that the BAM file contains MM/ML tags.

.. code-block:: zsh

   loreme check-tags Kazu_090722_aligned.bam

Pileup
------

The pileup step generates a bedMethyl file from an aligned BAM file.

Before running modkit, you will need to decompress the FASTA file.

.. code-block:: zsh

   gunzip Kazu_090722.softmasked.fasta.gz

Then you can generate a pileup

.. code-block:: zsh

   loreme modkit-pileup Kazu_090722.softmasked.fasta Kazu_090722_aligned.bam Kazu_090722.bed

.. note::

   See :zsh:`loreme modkit-pileup --help` for additional options. On a HPC
   system you may want to use additional threads with the :zsh:`-t` flag.

Postprocessing
--------------

See also the :ref:`pb_postprocessing` section for examples of postprocessing analysis that can be applied to bedMethyl files.

**Calculate mean methylation level**

.. code-block:: zsh

   loreme mean --total Kazu_090722.bed

.. code-block:: none

   Group        Methylation level (%)
   0    79.64114117704054

.. note::

    This and subsequent steps represent postprocessing that can be applied to either Pacific Biosciences or Oxford Nanopore datasets.

**Methylation level of promoters**

To calculate methylation levels of promoter regions:

.. code-block:: zsh

   loreme promoter Kazu_090722.primary_high_confidence.gff3.gz \
     Kazu_090722.bed --hist Kazu_090722_promoter.svg > Kazu_090722_promoter.bed
   head Kazu_090722_promoter.bed

.. code-block:: none

   ctg1	159758	161758	Kazu_090722.ctg1.g000010	86.36	-
   ctg1	256072	258072	Kazu_090722.ctg1.g000030	64.29	+
   ctg1	321679	323679	Kazu_090722.ctg1.g000050	76.79	-
   ctg1	397819	399819	Kazu_090722.ctg1.g000060	45.23	+
   ctg1	437295	439295	Kazu_090722.ctg1.g000070	100.00	+
   ctg1	444908	446908	Kazu_090722.ctg1.g000080	59.09	-
   ctg1	499196	501196	Kazu_090722.ctg1.g000100	97.64	-
   ctg1	547954	549954	Kazu_090722.ctg1.g000110	100.00	+
   ctg1	577225	579225	Kazu_090722.ctg1.g000120	77.38	-
   ctg1	662637	664637	Kazu_090722.ctg1.g000130	100.00	-

.. figure:: _static/Kazu_090722_hist_promoter.*
   :alt: Histogram of methylation levels of promoter regions.

   Histogram of methylation levels of promoter regions.

**Methylation level of gene bodies**

To calculate methylation levels of gene bodies:

.. code-block:: zsh

   loreme gene-body Kazu_090722.primary_high_confidence.gff3.gz \
     Kazu_090722.bed --hist Kazu_090722_gene_body.svg > Kazu_090722_gene_body.bed
   head Kazu_090722_gene_body.bed

.. code-block:: none

   ctg1	159267	159758	Kazu_090722.ctg1.g000010	95.00	-
   ctg1	237075	241223	Kazu_090722.ctg1.g000020	92.05	-
   ctg1	258072	259127	Kazu_090722.ctg1.g000030	100.00	+
   ctg1	320906	321679	Kazu_090722.ctg1.g000050	66.67	-
   ctg1	399819	411211	Kazu_090722.ctg1.g000060	83.36	+
   ctg1	439295	440181	Kazu_090722.ctg1.g000070	77.78	+
   ctg1	444411	444908	Kazu_090722.ctg1.g000080	100.00	-
   ctg1	456252	477732	Kazu_090722.ctg1.g000090	89.00	+
   ctg1	498921	499196	Kazu_090722.ctg1.g000100	100.00	-
   ctg1	549954	558103	Kazu_090722.ctg1.g000110	98.23	+

.. figure:: _static/Kazu_090722_hist_gene_body.*
   :alt: Histogram of methylation levels of promoter regions.

   Histogram of methylation levels of promoter regions.

**Gene methylation profile**

To plot a profile of methylation across gene bodies:

.. code-block:: zsh

   loreme plot-genes Kazu_090722.primary_high_confidence.gff3.gz \
     Kazu_090722.bed Kazu_090722_genes.svg

.. figure:: _static/Kazu_090722_genes.*
   :alt: Methylation profile across gene bodies.

   Methylation profile across gene bodies.
