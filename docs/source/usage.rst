Usage
=====

.. role:: zsh(code)
   :language: zsh

See the full help text with :zsh:`loreme --help`:

.. code-block:: zsh

   usage: loreme [-h] [--version]
                 {download-pbcpg,download-dorado,download-example,pbcpg,pbcpg-check,pbcpg-align,pbcpg-extract,dorado,dorado-check,dorado-convert,dorado-align,mean,promoter,gene-body,plot,plot-genes,plot-repeats,intersect}
                 ...
   
   Analysis of DNA methylation signals from `Pacific Biosciences <https://www.pacb.com/technology/hifi-
   sequencing/>`_ or `Oxford Nanopore <https://nanoporetech.com/applications/dna-nanopore-sequencing>`_ long
   read sequencing data.
   
   positional arguments:
     {download-pbcpg,download-dorado,download-example,pbcpg,pbcpg-check,pbcpg-align,pbcpg-extract,dorado,dorado-check,dorado-convert,dorado-align,mean,promoter,gene-body,plot,plot-genes,plot-repeats,intersect}
       download-pbcpg      download pb-CpG-tools
       download-dorado     download dorado
       download-example    download an example dataset for pbcpg
       pbcpg               run full pbcpg pipeline
       pbcpg-check         check BAM for MM/ML tags
       pbcpg-align         align BAM to reference
       pbcpg-pileup       pileup methylation calls from aligned PB reads
       dorado              run full dorado pipeline
       dorado-check        check GPU availability
       dorado-convert      convert FAST5 to POD5
       dorado-align        align dorado basecalls
       mean                calculate average methylation across chromosomes or in total
       promoter            quantify promoter methylation
       gene-body           quantify gene body methylation
       plot                plot methylation across chromosomes
       plot-genes          plot methylation profiles over genomic features
       plot-repeats        plot methylation profiles over genomic features
       intersect           intersect two or more bedmethyl files
   
   options:
     -h, --help            show this help message and exit
     --version             show program's version number and exit
